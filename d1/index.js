//Notes: 
// The Document refers to the whole webpage
//To access specific Object models from document we can use:

//document.querySelector('#txt-first-name')

//document.getElementById('txt-first-name')

//documents.getElementsByClassName('txt-inputs')

//document.getElementsByTagName('input')

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name')

/*txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`

})

txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
})*/

//Multiple Listeners can also be assigned to the same event
txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value)//similar to the txtFirstName.value
})

txtLastName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value)//similar to the txtFirstName.value
})

const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);